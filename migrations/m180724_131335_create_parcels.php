<?php

use yii\db\Migration;

/**
 * Handles the creation for table `parcels`.
 */
class m180724_131335_create_parcels extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('parcels', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'culture' => $this->string(100),
            'area' => $this->float(10,2),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('parcels');
    }
}
