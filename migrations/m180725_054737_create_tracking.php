<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tracking`.
 * Has foreign keys to the tables:
 *
 * - `tractors`
 * - `parcels`
 */
class m180725_054737_create_tracking extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tracking', [
            'id' => $this->primaryKey(),
            'tractor_id' => $this->integer()->notNull(),
            'plot_id' => $this->integer()->notNull(),
            'processing_date' => $this->date()->notNull(),
            'area' => $this->float()->notNull(),
        ]);

        // creates index for column `tractor_id`
        $this->createIndex(
            'idx-tracking-tractor_id',
            'tracking',
            'tractor_id'
        );

        // add foreign key for table `tractors`
        $this->addForeignKey(
            'fk-tracking-tractor_id',
            'tracking',
            'tractor_id',
            'tractors',
            'id',
            'CASCADE'
        );

        // creates index for column `plot_id`
        $this->createIndex(
            'idx-tracking-plot_id',
            'tracking',
            'plot_id'
        );

        // add foreign key for table `parcels`
        $this->addForeignKey(
            'fk-tracking-plot_id',
            'tracking',
            'plot_id',
            'parcels',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tractors`
        $this->dropForeignKey(
            'fk-tracking-tractor_id',
            'tracking'
        );

        // drops index for column `tractor_id`
        $this->dropIndex(
            'idx-tracking-tractor_id',
            'tracking'
        );

        // drops foreign key for table `parcels`
        $this->dropForeignKey(
            'fk-tracking-plot_id',
            'tracking'
        );

        // drops index for column `plot_id`
        $this->dropIndex(
            'idx-tracking-plot_id',
            'tracking'
        );

        $this->dropTable('tracking');
    }
}
