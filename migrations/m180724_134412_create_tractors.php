<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tractors`.
 */
class m180724_134412_create_tractors extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tractors', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tractors');
    }
}
