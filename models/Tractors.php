<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tractors".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Tracking[] $trackings
 */
class Tractors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tractors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrackings()
    {
        return $this->hasMany(Tracking::className(), ['tractor_id' => 'id']);
    }
}
