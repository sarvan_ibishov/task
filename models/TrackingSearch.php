<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tracking;

/**
 * TrackingSearch represents the model behind the search form about `app\models\Tracking`.
 */
class TrackingSearch extends Tracking
{
    public $tractor;
    public $plot;
    public $culture;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tractor_id', 'plot_id'], 'integer'],
            [['processing_date','tractor','plot','culture'], 'safe'],
            [['area'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tracking::find();

        // add conditions that should always apply here
        $query->joinWith(['tractor t','plot p']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'tractors.name' => $this->tractor,
//            'plot_id' => $this->plot_id,
//            'processing_date' => $this->processing_date,
//            'area' => $this->area,
//        ]);

        $query->andFilterWhere(['tracking.id' => $this->id]);
        $query->andFilterWhere(['like', 't.name', $this->tractor]);
        $query->andFilterWhere(['like', 'p.name', $this->plot]);
        $query->andFilterWhere(['like', 'p.culture', $this->culture]);
        $query->andFilterWhere(['tracking.processing_date' => $this->processing_date]);
//        $query->andFilterWhere(['like', 'tbl_person.last_name', $this->last_name]);
//        $query->andFilterWhere(['tbl_person.country_id' => $this->country_id]);
//        $query->andFilterWhere(['tbl_person.parent_id' => $this->parent_id]);

        return $dataProvider;
    }
}
