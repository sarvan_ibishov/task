<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parcels".
 *
 * @property integer $id
 * @property string $name
 * @property string $culture
 * @property double $area
 */
class Parcels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parcels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['area'], 'number'],
            [['name', 'culture'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'culture' => 'Culture',
            'area' => 'Area',
        ];
    }

    public static function getselectedplotarea($id)
    {
        $row = (new \yii\db\Query())
            ->select(['area'])
            ->from('parcels')
            ->where(['id' => (int)$id])
            ->one();
        return $row['area'];
    }
}
