<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tracking".
 *
 * @property integer $id
 * @property integer $tractor_id
 * @property integer $plot_id
 * @property string $processing_date
 * @property double $area
 *
 * @property Parcels $plot
 * @property Tractors $tractor
 */
class Tracking extends \yii\db\ActiveRecord
{
    public $selected_plot_area; //fictive new attribute
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tractor_id', 'plot_id', 'processing_date','area'], 'required'],
            [['tractor_id', 'plot_id'], 'integer'],
            [['processing_date'], 'safe'],
            [['area'], 'number'],
            ['area', function ($attribute, $params) {
                //print_r($this->selected_plot_area);exit;
                if ($this->$attribute>$this->selected_plot_area) {
                    $this->addError($attribute, 'Area that should not exceed the area of the selected plot');
                }
            }],
            [['plot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parcels::className(), 'targetAttribute' => ['plot_id' => 'id']],
            [['tractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tractors::className(), 'targetAttribute' => ['tractor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tractor_id' => 'Tractor',
            'plot_id' => 'Plot',
            'processing_date' => 'Processing Date',
            'area' => 'Area',
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlot()
    {
        return $this->hasOne(Parcels::className(), ['id' => 'plot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTractor()
    {
        return $this->hasOne(Tractors::className(), ['id' => 'tractor_id']);
    }


    public function beforeValidate()
    {
        if (parent::beforeValidate()) {

            $this->selected_plot_area = Parcels::getselectedplotarea($this->plot_id);
            return true;
        }
        return false;
    }

}
