<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tractors */

$this->title = 'Create Tractors';
$this->params['breadcrumbs'][] = ['label' => 'Tractors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tractors-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
