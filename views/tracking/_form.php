<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Tracking */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="tracking-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tractor_id')->dropDownList(
        $tractors,
        ['prompt' => 'Select']
    ); ?>

    <?= $form->field($model, 'plot_id')->dropDownList(
        $plots,
        ['prompt' => 'Select']); ?>


<!--    --><?//= $form->field($model, 'processing_date')->label() ?>
    <?= $form->field($model, 'processing_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => ''],

        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
    ]); ?>
    <?= $form->field($model, 'area', ['options' => ['class' => 'form-group1']])->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
