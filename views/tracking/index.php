<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrackingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trackings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tracking', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //   'id',
            // 'tractor_id',
            [
                'attribute' => 'tractor',
                'value' => 'tractor.name'
            ],
            [
                'attribute' => 'plot',
                'value' => 'plot.name'
            ],
            [
                'attribute' => 'culture',
                'value' => 'plot.culture'
            ],
            [
                'attribute' => 'processing_date',
                'value' => 'processing_date',
                'filter' => \kartik\date\DatePicker::widget([
                    'name' => 'TrackingSearch[processing_date]',
                    'value' => Yii::$app->request->get('TrackingSearch')['processing_date'],
                    'model' => $searchModel,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]]),
                //'format' => 'html',
            ],
            //   'plot_id',
            //'processing_date',
           // 'area',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
