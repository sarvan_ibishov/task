1. git clone https://sarvan_ibishov@bitbucket.org/sarvan_ibishov/task.git
2. composer install
3. php yii migrate

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/task/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=task',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```